Google Earth Pro compatible maps of sea ice concentration and primary productivity in 2016-07-01 for Green Edge 2016 for Gustavo. The input primary productivity file can be found at [https://goo.gl/zGsSGW](). The input file is also at /Volumes/output-prod/Takuvik/Teledetection/Couleur/SORTIES/36_0_0/NOCLIM/2016/183/AM2016183_PP.nc on the Takuvik shared resources.

The dependencies of this project are:

* R and the ncdf4 R package.
* GMT4. See [https://gitlab.com/Takuvik/resources_public/blob/master/GMT4.md]().
