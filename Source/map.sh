#!/bin/bash
# file: map.sh
# author: Maxime Benoit-Gagne
# creation: 2 September 2017
#
# Usage: ./map.sh filename
#        where infile = NetCDF file in full ISIN 45 North format with
#                       variables lat, lon and PP_oc
# 
# description: Create maps.
# keywords: map.

#############################################################################
# main

if [[ $# -eq 1 ]]
then
    filename=$1
    filename="${filename%.*}"
else
    echo "Usage: ./map.sh infile
        where filename = NetCDF file in full ISIN 45 North format with
                       variables lat, lon and PP_oc"
    exit 1
fi

gmtset COLOR_FOREGROUND darkred COLOR_BACKGROUND black
region=-Rg-80/-45/60/80
J=-JX14d # good
makecpt -V -Crainbow -T10/1000/2 -Z -Qo  > scale_PP_oc.cpt
cmd="Rscript ./call_NCTakuvik_2_GMT_txt.R ${filename}.nc ${filename}.txt PP_oc"
echo "${cmd}"
${cmd}
xyz2grd $region ${filename}.txt -G${filename}.grd -I5k -F -V
grdview ${filename}.grd -Cscale_PP_oc.cpt ${region} $J -K -P -Ts -V > \
	${filename}.ps
pscoast $region $J -Df -K -O -P -V -W \
	--BASEMAP_TYPE=inside --FRAME_PEN=0p >> \
 	${filename}.ps
# psscale: Plot gray scale or color scale on maps.
# Append to the PostScript (ps) file.
pos=11c/3c/3c/.35c
prodname='PP'
units='mgC.m@+-2@+.d@+-1@+'
colorbar_par=":${prodname}:/:${units}:"
scale=scale_PP_oc.cpt
psscale -D${pos} -B${colorbar_par} -C${scale} -E -Li -O -V \
 	>> ${filename}.ps
ps2raster ${filename}.ps -A -Tj -V
ps2raster ${filename}.ps -A -TG -V -W+k

# Modify the field name.
filename_basename=`basename ${filename}`
echo "${filename_basename}"
sed -i "" \
    "s/GMT\ KML\ Document/${filename_basename}\.kml/" \
    ${filename}.kml
sed -i "" \
    "s/GMT Image Overlay/${filename_basename}.png/" \
    ${filename}.kml

# To have the basename of the .png file instead of the absolute name for
# portability.
sed -i "" \
    "s/<href>.*<\/href>/<href>${filename_basename}.png<\/href>/" \
    ${filename}.kml

# Archive
filename_dirname=`dirname ${filename}`
cd ${filename_dirname}
zip ${filename_basename}.zip\
    ${filename_basename}.kml\
    ${filename_basename}.png



